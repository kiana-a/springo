package com.example.springo.repository;

import com.example.springo.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User,Long> {

    List<User> findByNameOrderByIdDesc(String name);


    List<User> findTopByCreated_at(String created_at);

    List<User> findDistinctByUsername(String username);

    List<User> findByUsername(String username);

    @Nullable
    List<User> findByNameAndUsernameIgnoreCase(String name, String username);


    List<User> findByNameOrderByUserIdDesc(String name);

    @Query(value = "select * from springo where id=?1", nativeQuery = true)
    List<User> findByUserId(String user_id);

    @Query("from User u where u.name= :name")
    List<User> findAllBy(@Param("name") String name);




}