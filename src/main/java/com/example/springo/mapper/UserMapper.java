package com.example.springo.mapper;


import com.example.springo.domain.User;
import com.example.springo.service.dto.UserDto;
import org.springframework.web.bind.annotation.Mapping;

import javax.xml.transform.Source;

@Mapper
public class UserMapper {

    //should I add a mapper interface just like ours on snapp box ?
    UserMapper INSTANCE = Mapper.getMapper(UserMapper.class);

    @Mapping(Source = "username", target = "user_id")
    UserDto userToUserDto(User user);



}
