package com.example.springo.domain;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "username")
    private String username;

    @Column(name = "user_id")
    private int userId;

    @Column(name = "credit")
    private double credit;

    @Column(name = "walletId")
    private int wallet_id;

    @Column(name = "created_at")
    private LocalDateTime created_at;

    @Column(name = "updated_at")
    private LocalDateTime updated_at;

    @Column(name = "deleted_at")
    private LocalDateTime deleted_at;

    public User(String name, String username) {
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public User setUsername(String username) {
        this.username = username;
        return this;
    }

    public double getCredit() {
        return credit;
    }

    public User setCredit(double credit) {
        this.credit = credit;
        return this;
    }

    public User() {
    }

    public User(String name, String username, double credit) {
        this.name = name;
        this.username = username;
        this.credit = credit;
    }

    public Long getId() {
        return id;
    }

    public User setId(Long id) {
        this.id = id;
        return this;
    }

    public int getUserId() {
        return userId;
    }

    public User setUserId(int userId) {
        this.userId = userId;
        return this;
    }

    public int getWallet_id() {
        return wallet_id;
    }

    public User setWallet_id(int wallet_id) {
        this.wallet_id = wallet_id;
        return this;
    }

    public LocalDateTime getCreated_at() {
        return created_at;
    }

    public User setCreated_at(LocalDateTime created_at) {
        this.created_at = created_at;
        return this;
    }

    public LocalDateTime getUpdated_at() {
        return updated_at;
    }

    public User setUpdated_at(LocalDateTime updated_at) {
        this.updated_at = updated_at;
        return this;
    }

    public LocalDateTime getDeleted_at() {
        return deleted_at;
    }

    public User setDeleted_at(LocalDateTime deleted_at) {
        this.deleted_at = deleted_at;
        return this;
    }
}
