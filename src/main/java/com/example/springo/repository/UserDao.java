package com.example.springo.repository;

import com.example.springo.domain.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

 public class UserDao implements Dao<User> {
    private List<User> users = new ArrayList<>();

    public UserDao(){
        users.add(new User("Kiana", "Kiana@snappbox.com"));
        users.add(new User("Test", "Test@snappbox.com"));
    }

    @Override
    public Optional<User> get(long id) {
        return Optional.ofNullable(users.get((int) id));
    }

    @Override
    public List<User> getAll() {
        return users;
    }

    @Override
    public void save(User user) {
        users.add(user);
    }


    @Override
    public void update(User user, String[] params) {
        user.setName(Objects.requireNonNull(params[0], "Name cannot be null"));
        user.setUsername(Objects.requireNonNull(params[1],"Username cannot be null"));
        users.add(user);
    }

    @Override
    public void delete(User user) {
        users.remove(user);
    }
}
