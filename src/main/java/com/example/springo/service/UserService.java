package com.example.springo.service;


import com.example.springo.domain.User;
import com.example.springo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class UserService implements IUserService{

    @Autowired
    private UserRepository userRepository;

    @Override
    public void deleteUser(User user) {
        userRepository.delete(user);
    }

    @Override
    public void saveRegisteredUser(User user) {
        userRepository.save(user);
    }


}
