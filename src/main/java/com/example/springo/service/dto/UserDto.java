package com.example.springo.service.dto;

import javax.validation.constraints.NotBlank;

public class UserDto {
    private int id;

    @NotBlank(message = "springo.com.name.is.null")
    private String name;

    @NotBlank(message = "springo.com.username.is.null")
    private String username;

    @NotBlank(message = "springo.com.walletId")
    private Integer walletId;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setWalletId(Integer walletId) {
        this.walletId = walletId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public Integer getWalletId() {
        return walletId;
    }

    public UserDto(int id, @NotBlank(message = "springo.com.name.is.null") String name, @NotBlank(message = "springo.com.username.is.null") String username, @NotBlank(message = "springo.com.walletId") Integer walletId) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.walletId = walletId;
    }
}
